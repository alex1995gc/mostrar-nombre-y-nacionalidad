<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejercicio 2</title>
	<style type="text/css">
		.color {
			color: red;
			font-weight: bold;
		}
		.subrayado {
			text-decoration: underline;
		}
	</style>
</head>
<body>
	<?php 
		$nombre = 'Alexis Gali';
		$nacionalidad ='Paraguay';
		echo "<p class=\"color\">$nombre</p>";
		print("<p class=\"subrayado\">$nacionalidad</p>");
	 ?>
</body>
</html>